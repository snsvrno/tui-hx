package tui;

#if ansi
import ansi.colors.Style;
import ansi.Paint.paint;
#end

class Formats {

	/**
	 * This exists in the case the user does not use `ansi` library.
	 * Don't want to make it a requirement.
	 *
	 * This shadows `ansi.Format.color` but just passes through the
	 * expression.
	 */
	inline public static macro function color(text:haxe.macro.Expr) {
		return text;
	}

	public static function tab(?n:Int = 1) {
		for (_ in 0 ... n) Sys.print("  ");
	}

	public static function space(?n:Int = 1) {
		for (_ in 0 ... n) Sys.print(" ");
	}

	//////////////////////////////////////////
	// inline coloring so that we have uniform
	// styling for different parts of the program

	inline public static function unknownCommand(cmd:String) : String {
		#if ansi return paint(cmd, Yellow, Style.Bold | Style.Underline);
		#else return cmd;
		#end
	}

	public static function option(text:String) : String {
		#if ansi return paint(text, Green);
		#else return text;
		#end
	}

	public static function value(text:String) : String {
		#if ansi return ansi.Paint.paintPreserve(text, Yellow);
		#else return text;
		#end
	}

	public static function parameter(text:String) : String{
		#if ansi return paint(text, Magenta);
		#else return text;
		#end
	}

	public static function parameterUsage(text : String) : String{
		#if ansi return
			paint("<", White, Style.Dim) +
			parameter(text) +
			paint(">", White, Style.Dim);
		#else return "<" + text + ">";
		#end
	}

	public static function command(text : String, ?style : Style = None) : String{
		#if ansi return paint(text, Blue, style);
		#else return text;
		#end
	}

	public static function types(anything : Dynamic) : String {
		switch(Type.typeof(anything)) {
			case TNull: return paint("null", Cyan);
			case TInt: return paint(anything + "", Green);
			case TFloat: return paint(anything + "", Blue);
			case TBool: return paint(anything + "", Magenta);
			case TObject:
				var keys = Reflect.fields(anything);

				var s = paint("{ ",White, Style.Dim);
				for (k in keys) {
					var value = Reflect.getProperty(anything,k);
					s += '${paint(k,Blue)} ${paint(":",White, Style.Dim)} ${types(value)}${paint(",",White, Style.Dim)}';
				}
				s += paint(" }",White, Style.Dim);
				return s;

			case TClass(c):
				if (Std.isOfType(anything, Array)) {
					var array = cast(anything, Array<Dynamic>);

					var s = paint("[ ",White, Style.Dim);
					for (a in array)
						s += '${types(a)}${paint(",",White, Style.Dim)}';
					s += paint(" ]",White, Style.Dim);

					return s;
				} else if (Std.isOfType(anything, String)) {
					return paint(anything + "", Yellow);
				}


				return paint(c + "", Red);

			case _:
				return anything.toString();
		}
	}
}
