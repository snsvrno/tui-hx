package tui.defaults;

import tui.types.Command;
import tui.types.Program;
import tui.types.Switch;

class Config implements Command {
	
	//////////////////////////////////////

	public var name = "config";
	public var description = "shows values and helps configure the settings";
	public var group = "core";
	public var manDescription = "Shows values and helps configure the settings. Runs the command with no arguements will show the current configuration";

	public var switches = [
		{ name: "default", long: "--default", description: "lists the default configuration with values" },
	];

	public var commands = [

	];

	private var conf:tui.types.ConfigObject;

	/////////////////////////////////////

	public function new(object:tui.types.ConfigObject) {
		conf = object;
	}

	public function run( ...args : String) {

		if (Switches.getFlag("default")) {

		}

		var arr = args.toArray();
		switch(arr.shift()) {

			case "set":
				var key = arr.shift();
				var value = arr.shift();
				conf.saveValue(null, key, value);

			// load the currently set configuration that is found in the
			// default path.
			case null:
				if (!conf.load()) {
					Sys.println('no local configuration found.');
					return;
				}

				for (key in conf.fields)
					outSettings(key, Reflect.getProperty(conf, key));

			case other:
				err(new error.EGeneric(c('unknown command $other')));

		}

	}

	inline private static function outSettings(key:String, value:Dynamic, ?description:String) {
		Sys.println(c('  $key ${paint("=>", Black, Dim)} $value'));
		if (description != null) Sys.print(" " + paint(description, White, Dim));
		Sys.println("");
	}


}
