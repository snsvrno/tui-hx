package tui.defaults;

using sn.ds.StringTools;
using ansi.colors.ColorTools;

import ansi.Paint.paint;
import ansi.colors.Style;

import tui.types.Command;
import tui.types.Program;
import tui.types.Switch;

import tui.Formats.*;
import tui.errors.*;

/**
	* a standard implementation of a help command, will be
	* added to the list of commands automatically if no help
	* command is found and there is at least one command
	*/
class Help implements Command {
	
	//////////////////////////////////////

	public var name = "help";
	public var description = "shows this screen and extended help information for each command";
	public var manDescription = "help can be used with each command to further describe the commands use and optional switches and parameters";
	public var group = "core";

	private var parent : Program;

	/////////////////////////////////////

	public function new(parent : Program) {
		this.parent = parent;
	}

	public function run( ...args : String) {
		// get the terminal size
		var size = ansi.Command.getSize();

		// we want to know the help of the whole program.
		if (args.length == 0) {

			functionNameLine(parent.name);
			Sys.println(parent.description);
			usage(parent.name, null, parent.usageCommandOverride);
			listCommands(size.c);
			listSwitches(size.c);

		} else {
			for (comm in parent.commands) {
				if (comm.check(args[0])) {
					functionNameLine(parent.name, args[0]);
					man(comm, size.c);
					return;
				}
			}
			err(new ECommand(unknownCommand(args[0])));
		}
	}

	public function man(cmd : Command, screenWidth : Int) {
		Sys.println(cmd.manDescription);
		usage(parent.name + " " + cmd.name, if (cmd.switches.length == 0) "" else null, "command");
		listCommands(screenWidth, cmd.commands);
		listSwitches(screenWidth, cmd.switches);
	}

	//////////////////////////////////////////////////////////////////////

	inline public function usage(cmd : String, ?switchText : String = "-s --switch", ?paramText : String = "command") {
		Sys.print("\n");
		tab();
		Sys.println(paint("Usage", White, Style.Bold) + ": " +
				command(cmd.toLowerCase()) + " " +
			  (if(switchText.length > 0) option(switchText) + " " else "") +
				parameterUsage(paramText)
			);
	}

	inline private function functionNameLine(name : String, ?cmd : String) {
		Sys.print(command(name, Style.Bold));
		if (cmd != null)
			Sys.print(" " + parameter(cmd));
		else
			Sys.print(" " + paint(parent.version, Yellow));
		Sys.print("\n");
	}

	inline private function header(text: String) {
		tab();
		Sys.println(paint(text, White, Style.Underline) + ":");
	}

	inline private function listCommands(screenWidth : Int, ?commands : Array<Command>) {

		if (commands == null) commands = parent.commands.copy();
		var maxNameLength = Math.floor(Math.max(getMaxLength(commands, "display"), getMaxLength(commands, "name")));

		// grouped items ///////////////////////////////
		var groups = [];
		for (c in commands) {
			if (!c.hidden && c.group != null && !groups.contains(c.group)) groups.push(c.group);
		}

		for (g in groups) {
			Sys.print("\n");
			header('${sn.ds.StringTools.capitalize(g)} Commands');

			var remove = [];
			for (i in 0 ... commands.length) if (commands[i].group == g) {
				displayCommand(commands[i], maxNameLength, screenWidth);
				remove.push(i);
			}
			while (remove.length > 0) commands.remove(commands[remove.pop()]);
		}

		// the rest of the items ////////////////////////////
		var count = 0;
		for (c in commands) if(!c.hidden) count += 1;

		// if this command has no commands then we can skip this section
		if (count > 0) {
			Sys.print("\n");
			header("Commands");

			for (comm in commands) displayCommand(comm, maxNameLength, screenWidth);
		}
	}

	inline private function displayCommand(comm:Command, maxNameLength:Int, screenWidth:Int) {
		// draws the name
		tab(2);
		Sys.print(parameter(if (comm.display != null) comm.display else comm.name).pad(maxNameLength));

		// draws the description
		tab();
		var pos = ansi.Command.cursorPosition();
		var remainingSpace = screenWidth - pos.c + 1;
		var lines = [];
		{
			var parts = comm.description.split(" ");
			var line = parts.shift();
			var linelength = ansi.colors.ColorTools.length(line);
			while (parts.length > 0) {
				var plength = ansi.colors.ColorTools.length(parts[0]);
				if (plength + linelength + 1 <= remainingSpace) {
					linelength += plength + 1;
					line += " " + parts[0];
				} else {
					lines.push(line);
					line = parts[0];
					linelength = plength;
				}
				parts.shift();
			}
			if (line.length > 0) lines.push(line);
		}

		//var lines = ansi.colors.ColorTools.splitLength(comm.description, screenWidth - pos.c + 1);
		for (i in 0 ... lines.length) {
			if (i != 0) space(pos.c - 1);
			Sys.println(lines[i]);
		}
	}

	inline private function listSwitches(screenWidth : Int, ?switches : Array<Switch>) {

		if (switches == null) switches = parent.globalSwitches;

		if (switches.length > 0) {

			var groups = [];
			for (s in switches) {
				var name = if (s.group == null) "null" else s.group;
				if(!groups.contains(name)) groups.push(name);
			}

			if (groups.length > 1) {
				for(g in groups) listSwitches(screenWidth, switches.filter((a) -> {
					if(a.group == null && g == "null") return true;
					else if(a.group != null) return a.group == g;
					return false;
				}));
			}

			// if we only have one group we just do them all.
			else {
				Sys.print("\n");
				var name = groups[0].capitalize() + " ";
				if (name == "Null ") name = "";
				header(name + "Switches");

				var maxNameLength = getMaxLength(switches, "name") + 1;
				var maxSwitchLength = getMaxLength(switches, "long") + 4;

				for (gs in switches) {
					tab(2);
					Sys.print(parameter(gs.name).pad(maxNameLength));

					tab();
					var text = "";
					if (gs.short != null) text += option(gs.short);
					if (text.length > 0 && gs.long != null) text += ", ";
					if (gs.long != null) text += option(gs.long);
					if (gs.value != null && gs.value == true)
						text += " " + value(paint("<", White, Style.Dim) + "value" + paint(">", White, Style.Dim));
					Sys.print(text.pad(maxSwitchLength));

					tab();
					var pos = ansi.Command.cursorPosition();
					var lines = ansi.colors.ColorTools.splitLength(gs.description, screenWidth - pos.c + 1);
					for (i in 0 ... lines.length) {
						if (i != 0) space(pos.c - 1);
						Sys.println(lines[i]);
					}
				}
			}

		}
	}

	private function getMaxLength(a : Array<Dynamic>, param : String) : Int {
		var length = 0;
		for (item in a) {
			var value = Reflect.getProperty(item, param);
			if (value != null) {
				var l = '$value'.length;
				if (l > length) length = l;
			}
		}
		return length;
	}
}
