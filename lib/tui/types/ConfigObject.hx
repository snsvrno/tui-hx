package tui.types;


/**
 * used to give access to a config object in the build it
 * config command implementation
 */
typedef ConfigObject = {
	var fields:Array<String>;
	var descriptions:Map<String, String>;

	function save(?path:String):Void;
	function saveValue(?path:String, key:String, value:Dynamic):Void;
	function load(?path:String):Bool;

	function getPath():String;
}
