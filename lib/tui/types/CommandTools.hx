package tui.types;

class CommandTools {

	public static function get(commands:Array<Command>, name:String) : Null<Command> {
		for (command in commands) if (command.check(name)) {
			return command;
		}
		return null;
	}

}
