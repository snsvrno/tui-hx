package tui;

import result.Result;
import error.Error;

// note, this is only working with a flat config and json for now.

class Config {

	private static var defaultValues:Null<Dynamic> = null;
	private static var path:Null<String> = null;

	public static function setup(defaults:Dynamic, path:String) {
		defaultValues = defaults;
		Config.path = path;
	}

	/**
	 * Helper function to load a configuration file to a typedef.
	 *
	 * Can return the following errors:
	 * error.file.EFileIsDir, error.file.EFileDoesNotExist
	 */
	public static function load(?path:String, ?loadInto:Dynamic) : Result<Dynamic, Error> {
		if (path == null && Config.path == null)
			return Error(new error.EGeneric("no path defined"));
		else if (path == null) path = Config.path;

		dbg(c('loading $path'));
		var config = if (loadInto != null) loadInto else { };

		if (!sys.FileSystem.exists(path))
			return Error(new error.file.EFileDoesNotExist(path));

		if (sys.FileSystem.isDirectory(path))
			return Error(new error.file.EFileIsDir(path));

		var content = sys.io.File.getContent(path);
		var parsed = haxe.Json.parse(content);
		fill(config, parsed, true);

		return Ok(config);
	}

	/**
	 * Will copy all the values from one dynamic to another.
	 *
	 * @param noNewValues Bool will only copy values if the key exists in the current
	 * `config`
	 */
	private static function fill(config:Dynamic, values:Dynamic, ?noNewValues:Bool = false) {
		var keys = Reflect.fields(values);
		for (key in keys) {
			if (Reflect.hasField(config, key) || noNewValues == false)
				Reflect.setProperty(config, key, Reflect.getProperty(values, key));
			else if (noNewValues)
				wrn(c('key $key is not defined, will ignore'));
		}
	}

}
