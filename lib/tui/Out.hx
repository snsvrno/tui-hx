package tui;

private enum Kind {
	Debug(sig:Signature);
	Error(sig:ErrorSignature);
	Warning(sig:Signature);
}

private enum abstract Level(Int) to Int from Int {
	var Error = 0;
	var Warning = 1;
	var Debug = 2;
}

typedef Signature = (text:String) -> Void;
typedef ErrorSignature = (e:error.Error) -> Void;
typedef QueItem = {
	kind:Level,
	?text:String,
	?e:error.Error,
};


//////////////////////////////////////

class Out {

	// this so that things will be recorded if there is nothing registered
	// yet, this will be persistent so that it will have the complete
	// record of items so adding different paths should still yield the same
	// list of events even if added later in the program.
	private static var que:Array<QueItem> = [];

	public static var level:Level = Error;

	private static var debugPaths:Map<Level, Array<Signature>> = new Map();
	private static var warningPaths:Map<Level, Array<Signature>> = new Map();
	private static var errorPaths:Map<Level, Array<ErrorSignature>> = new Map();

	public static function addPath(kind:Kind, ?lvl:Level) {
		switch(kind) {
			case Debug(sig):
				if (lvl == null) lvl = Debug;
				var paths = debugPaths.get(lvl);
				if (paths == null) paths = [];
				paths.push(sig);
				debugPaths.set(lvl, paths);
				for (i in 0 ... que.length) {
					if (cast(que[i].kind, Int) == cast(Debug, Int)) sig(que[i].text);
				}

			case Warning(sig):
				if (lvl == null) lvl = Warning;
				var paths = warningPaths.get(lvl);
				if (paths == null) paths = [];
				paths.push(sig);
				warningPaths.set(lvl, paths);
				for (i in 0 ... que.length) {
					if (cast(que[i].kind, Int) == cast(Warning, Int)) sig(que[i].text);
				}

			case Error(sig):
				if (lvl == null) lvl = Error;
				var paths = errorPaths.get(lvl);
				if (paths == null) paths = [];
				paths.push(sig);
				errorPaths.set(lvl, paths);
				for (i in 0 ... que.length) {
					if (cast(que[i].kind, Int) == cast(Error, Int)) sig(que[i].e);
				}
		}
	}

	public static function debug(text : String) {
		que.push({kind:Debug, text:text});
		for (lvl in 0 ... level + 1) {
			var paths = debugPaths.get(lvl);
			if (paths != null) for (path in paths) path(text);
		}
	}

	public static function error(e : error.Error) {
		que.push({kind:Error, e:e});
		for (lvl in 0 ... level + 1) {
			var paths = errorPaths.get(lvl);
			if (paths != null) for (path in paths) path(e);
		}
	}

	public static function warning(text : String) {
		que.push({kind:Warning, text:text});
		for (lvl in 0 ... level + 1) {
			var paths = warningPaths.get(lvl);
			if (paths != null) for (path in paths) path(text);
		}
	}
}
