package tui.errors;

class ESwitch implements error.Error {
	public var switchName : String;

	public function new(switchName : String) {
		this.switchName = switchName;
	}

	public function toString() : String {
		return c('$switchName switch not found');
	}
}
