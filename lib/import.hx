#if ansi
// if the ansi library is used, some coloring functions
// that we use
import ansi.Format.color as c;
import ansi.Paint.paint;
import ansi.Paint.paintPreserve;
import ansi.colors.Style;

#else
import tui.Formats.color as c;
#end

import tui.Out.debug as dbg;
import tui.Out.error as err;
import tui.Out.warning as wrn;
